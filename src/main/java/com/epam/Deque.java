package com.epam;

import java.util.ArrayDeque;

public class Deque {
    public static void main(String[] args) {
        ArrayDeque<Integer> deque = new ArrayDeque<Integer>();
        for (int i = 0; i < 5; i++)
            deque.add(i);
        for (int i = 5; i < 10; i++)
            deque.addFirst(i);
        for (int i = 10; i < 15; i++)
            deque.addLast(i);
        for (int i : deque)
            System.out.println(i);
    }
}
