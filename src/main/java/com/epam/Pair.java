package com.epam;

public class Pair implements Comparable<Pair> {
    private String first;
    private String second;

    public Pair(String first, String second) {
        this.first = first;
        this.second = second;
    }

    public int compareTo(Pair other) {
        return first.compareTo(other.first);
    }

    @Override
    public String toString() {
        return "Pair{" +
                "first='" + first + '\'' +
                ", second='" + second + '\'' +
                '}';
    }
}

